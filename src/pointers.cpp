#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;

	cout<<"Values of the variables"<<endl;
	cout<<"x = "<<x<<", y = "<<y<<endl;
	cout<<"a = "<<*a<<", b = "<<*b<<endl;
	cout<<"c = "<<c<<", d = "<<d<<endl;
	
	cout<<endl<<endl;
	
	cout<<"Addresses of the variable"<<endl;
	cout<<"x = "<<&x<<", y = "<<&y<<endl;
	cout<<"a = "<<&a<<", b = "<<&b<<endl;
	cout<<"c = "<<&c<<", d = "<<&d<<endl;
	



}
